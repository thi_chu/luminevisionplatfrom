using System;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.UI
{
    public class HeathBar : MonoBehaviour
    {
        [SerializeField] private Image heathFill;

        private void Update()
        {
            
        }


        public void UpdateHp(int currentHp, int maxHp)
        {
            heathFill.fillAmount =(float) currentHp / maxHp;
        }
    }
}
