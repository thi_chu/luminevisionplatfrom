using _Scripts.Manager;
using _Scripts.Player;
using UnityEngine;

namespace _Scripts.Enemies
{
    public class BossAttack : MonoBehaviour
    {
        [SerializeField] private EnemyStatus status;
        [SerializeField] private float meleAtkRange = 2f;
        [SerializeField] private float meleAtkDamage = 20f;
        [SerializeField] private float atkCooldown = 2f;

        [SerializeField] private Transform atkPoint;
        [SerializeField] private LayerMask playerLayer;

        private Transform _player;
        private float _atkTimer;

        // Start is called before the first frame update
        void Start()
        {
            _player = GameObject.FindGameObjectWithTag("Player").transform;
        }

        // Update is called once per frame
        void Update()
        {
            if (PlayerInSight() && _player != null)
            {
                _atkTimer -= Time.deltaTime;
                if (_atkTimer <= 0)
                {
                    _atkTimer = atkCooldown;
                    status.GetAnim.SetTrigger("meleAtk");
                    AudioManager.Instance.PlaySfx("BossAttack");
                }
            }
        }

        /// goi khi animation meleAtk di toi frame cuoi
        private void AttackPlayer()
        {
            _player.GetComponent<PlayerStatus>().TakeDamage((int)meleAtkDamage);
        }

        
        /// Check player co trong tam danh ko
        private bool PlayerInSight()
        {
            var hit = Physics2D.CircleCast(
                atkPoint.position, 
                meleAtkRange, 
                Vector2.left, 
                0f, 
                playerLayer);
            return hit.collider != null;
        }

        private void OnDrawGizmos()
        {
            var hit = Physics2D.CircleCast(
                atkPoint.position, 
                meleAtkRange, 
                Vector2.left, 
                0f, 
                playerLayer);
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(atkPoint.position,meleAtkRange);
        }
    }
}