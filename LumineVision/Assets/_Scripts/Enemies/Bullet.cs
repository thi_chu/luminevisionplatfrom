using System;
using _Scripts.Player;
using UnityEngine;

namespace _Scripts.Enemies
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private float speed = 3f;
        [SerializeField] private float lifeTime = 2f;
        [SerializeField] private int damage;
        [SerializeField] private bool isFlyEnemy;
        [SerializeField] private Rigidbody2D rgb2D;

        private float _lifeTimer;
        private GameObject _player;

        private void Start()
        {
            _player = GameObject.FindGameObjectWithTag("Player");
            
            if (isFlyEnemy)
            {
                var direction = _player.transform.position - transform.position;
                rgb2D.velocity = new Vector2(direction.x, direction.y).normalized * speed;

                float rotation = Mathf.Atan2(-direction.y, -direction.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.Euler(0, 0, rotation - 90);

            }
        }

        // Update is called once per frame
        void Update()
        {
            if (!isFlyEnemy)
            {
                // di chuyen fileball theo huong da xac dinh
                var movementSpeed = speed * Time.deltaTime * -transform.localScale.x;
                transform.Translate(movementSpeed, 0, 0);
            }

            /*
             * qua thoi gian quy dinh ma chua va cham se huy fireball
             */
            _lifeTimer += Time.deltaTime;
            if (_lifeTimer >= lifeTime)
            {
                Deactivate();
            }
        }

        private void OnTriggerEnter2D(Collider2D coll)
        {
            if (coll.CompareTag("Player"))
            {
                coll.GetComponent<PlayerStatus>().TakeDamage(damage);
                Deactivate();
            }
        }

        /// xoa fireball
        private void Deactivate()
        {
            Destroy(gameObject);
        }
    }
}