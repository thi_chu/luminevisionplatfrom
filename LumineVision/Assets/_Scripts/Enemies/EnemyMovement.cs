using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace _Scripts.Enemies
{
    public class EnemyMovement : MonoBehaviour
    {
        [Header("Objects")]
        [SerializeField] private Transform enemy;
        [SerializeField] private EnemyStatus controller;
        [SerializeField] private List<GameObject> movePoints;

        [Header("Optional")] 
        [SerializeField] private float speed;
        [SerializeField] private float idleDuration;


        private bool _isMoveLeft;
        private float _idleTimer;
        private Vector3 _initScale;

        private enum MovementSate
        {
            Idle,
            Running,
        }

        private MovementSate _sate;
        private static readonly int MoveState = Animator.StringToHash("moveState");

        // Start is called before the first frame update
        void Start()
        {
            _initScale = enemy.localScale;
        }

        // Update is called once per frame
        void Update()
        {
            if (enemy == null)
            {
                return;
            }

            if (controller.IsDead)
            {
                return;
            }
            
            if (_isMoveLeft)
            {
                if (enemy.position.x >= movePoints.First().transform.position.x)
                {
                    MoveInDirection(-1);
                }
                else
                {
                    DirectionChange();
                }
            }
            else
            {
                if (enemy.position.x <= movePoints.Last().transform.position.x)
                {
                    MoveInDirection(1);
                }
                else
                {
                    DirectionChange();
                }
            }
        }

        private void MoveInDirection(int direction)
        {
            if (controller.IsHurt)
            {
                _idleTimer += Time.deltaTime;

                if (!(_idleTimer >= idleDuration))
                {
                    _sate = MovementSate.Idle;
                    controller.GetAnim.SetInteger(MoveState, (int) _sate);
                    return;
                }

                controller.CanMove();
                MoveEnemy(direction);
            }
            else
            {
                MoveEnemy(direction);
            }
        }

        private void MoveEnemy(int direction)
        {
            _idleTimer = 0;

            _sate = MovementSate.Running;
            controller.GetAnim.SetInteger(MoveState, (int) _sate);

            var position = enemy.position;

            enemy.localScale = new Vector3(_initScale.x * direction, _initScale.y, _initScale.z);
            enemy.position = new Vector3(
                position.x + Time.deltaTime * direction * speed,
                position.y,
                position.z);
        }

        private void DirectionChange()
        {
            _sate = MovementSate.Idle;
            controller.GetAnim.SetInteger(MoveState, (int) _sate);


            _idleTimer += Time.deltaTime;

            if (_idleTimer >= idleDuration)
            {
                _isMoveLeft = !_isMoveLeft;
            }
        }

        private void OnDisable()
        {
            if (enemy == null)
            {
                return;
            }
            
            _sate = MovementSate.Idle;
            controller.GetAnim.SetInteger(MoveState, (int) _sate);
        }
    }
}