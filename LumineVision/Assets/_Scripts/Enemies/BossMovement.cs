using System;
using UnityEngine;

namespace _Scripts.Enemies
{
    public class BossMovement : MonoBehaviour
    {
        [SerializeField] private EnemyStatus status;
        [SerializeField] private LayerMask playerLayer;
        [SerializeField] private float speed = 2f;
        [SerializeField] private float detectedRadius = 10f;

        [HideInInspector] public bool isChaseToPlayer;

        private Transform _player;
        private Vector3 _initScale;


        /**
         *  là một kiểu dữ liệu đại diện cho một phương thức có thể được gọi.
         *  Delegates cho phép bạn định nghĩa các phương thức có thể được truyền
         *  như là tham số cho các phương thức khác hoặc được gán cho các sự kiện.
         *  Delegates rất hữu ích trong việc tạo các sự kiện và các callback.
         */
        private delegate void PlayerDetectedHandler(bool isDetected, Transform player);

        private event PlayerDetectedHandler OnPlayerDetected; // Định nghĩa một sự kiện dựa trên delegate

        // Start is called before the first frame update
        void Start()
        {
            _player = GameObject.FindGameObjectWithTag("Player").transform;
            _initScale = transform.localScale;
        }

        // Update is called once per frame
        void Update()
        {
            CheckPlayerInRange();
            if (isChaseToPlayer)
            {
                FollowPlayer();
            }
        }

        private void CheckPlayerInRange()
        {
            Collider2D coll = Physics2D.OverlapCircle(transform.position, detectedRadius, playerLayer);
            if (coll != null && coll.CompareTag("Player"))
            {
                isChaseToPlayer = true;
                OnPlayerDetected?.Invoke(true, _player);
                return;
            }

            isChaseToPlayer = false;
            OnPlayerDetected?.Invoke(false, _player);
            status.GetAnim.SetBool("isWalk", false);
        }

        private void FollowPlayer()
        {
            var pos = transform.position;
            Vector2 direction = (_player.position - pos).normalized;
            transform.localScale = direction.x > 0
                ? new Vector3(1 * _initScale.x, _initScale.y, _initScale.z)
                : new Vector3(-1 * _initScale.x, _initScale.y, _initScale.z);
            if (Vector2.Distance(_player.position, transform.position) >= 4.5f)
            {
                transform.position = new Vector3(
                    pos.x + (Time.deltaTime * direction.x * speed),
                    pos.y,
                    pos.z);
                status.GetAnim.SetBool("isWalk", true);
            }
            else
            {
                status.GetAnim.SetBool("isWalk", false);
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, detectedRadius);
        }
    }
}