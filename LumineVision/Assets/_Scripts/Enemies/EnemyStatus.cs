using _Scripts.Manager;
using _Scripts.UI;
using UnityEngine;

namespace _Scripts.Enemies
{
    public class EnemyStatus : MonoBehaviour
    {

        [Header("Enemy")]
        [SerializeField] private Transform enemy;
        [SerializeField] private GameObject hpBar;
        [SerializeField] private Animator animator;
        [SerializeField] private BoxCollider2D coll2D;
        [SerializeField] private HeathBar heathBar;
        
        [Header("Optional")]
        [SerializeField] private int maxHp;
        [SerializeField] private bool isBoss;


        private int _currentHp;
        private bool _isDead;
        private bool _isHurt;

        public Transform GetEnemy => enemy;
        public Animator GetAnim => animator;
        public Collider2D GetColl2D => coll2D;
        public bool IsHurt => _isHurt;
        public bool IsDead => _currentHp <= 0;
        // Start is called before the first frame update
        void Start()
        {
            _currentHp = maxHp;
            // heathBar.UpdateHp(_currentHp,maxHp);
        }

        public void TakeDamage(int dmg)
        {
            if (_isDead)
            {
                return;
            }

            _currentHp -= dmg;
            

            if (IsDead)
            {
                Die();
            }
            else
            {
                hpBar.SetActive(true);
                heathBar.UpdateHp(_currentHp,maxHp);
                animator.SetTrigger("hurt");
                _isHurt = true;
            }
        }

        private void Die()
        {
            if (!_isDead)
            {
                hpBar.SetActive(false);
                _isDead = true;
                animator.SetTrigger("die");
                if (!isBoss)
                {
                    coll2D.enabled = false;
                }
                else
                {
                    gameObject.layer = LayerMask.NameToLayer("Default");
                    AudioManager.Instance.GetmusicSource.Stop();
                    AudioManager.Instance.PlaySfx("Compelete");
                    UIManager.Instance.GameOverPanel();
                }
                OnDestroy();
            }
        }

        public void CanMove()
        {
            _isHurt = false;
        }
        private void OnDestroy()
        {
            Destroy(enemy.gameObject,0.85f);
        }
    }
}
