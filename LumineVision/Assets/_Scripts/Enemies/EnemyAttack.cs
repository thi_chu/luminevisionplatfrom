using _Scripts.Player;
using UnityEngine;

namespace _Scripts.Enemies
{
    public class EnemyAttack : MonoBehaviour
    {
        [Header("Scripts")]
        [SerializeField] private EnemyStatus enemy;
        [SerializeField] private EnemyMovement enemyMove;

        [Header("Optional")]
        [SerializeField] private Transform attackPoint;
        [SerializeField] private GameObject bullet;
        [SerializeField] private float atkDuration;
        [SerializeField] private bool isFlyEnemy;
        
        [Header("Hit Box")] 
        [SerializeField] private float hitBoxRangeX = 1f;
        [SerializeField] private float hitBoxRangeY = 1f;
        [SerializeField] private float hitBoxDistance;
        [SerializeField] private LayerMask playerLayer;

        private float _atkCoolDownTime;

        // Start is called before the first frame update
        void Start()
        {
            _atkCoolDownTime = atkDuration;
        }

        // Update is called once per frame
        void Update()
        {
            _atkCoolDownTime -= Time.deltaTime;
            if (PlayerInSight() && !PlayerStatus.Instance.ChechDead())
            {
                if (_atkCoolDownTime <= 0)
                {
                    _atkCoolDownTime = atkDuration;
                    enemy.GetAnim.SetTrigger("attack");
                }
            }

            if (enemyMove != null)
            {
                enemyMove.enabled = !PlayerInSight();
            }
        }

        /// goi khi animation attack di toi frame cuoi
        private void Attack()
        {
            var thisScale = transform.localScale;
            GameObject currenBullet;
            currenBullet = Instantiate(bullet, attackPoint.position, !isFlyEnemy ? attackPoint.rotation : Quaternion.identity);
            currenBullet.transform.localScale = new Vector3(thisScale.x, thisScale.y, thisScale.z);
        }

        private bool PlayerInSight()
        {
            var bounds = enemy.GetColl2D.bounds;
            var originBox = bounds.center + enemy.transform.right * (hitBoxRangeX * enemy.transform.localScale.x * hitBoxDistance);;
            var sizeHitBox = new Vector3(hitBoxRangeX * bounds.size.x, hitBoxRangeY * bounds.size.y, bounds.size.z);
            RaycastHit2D hitBox = Physics2D.BoxCast(
                originBox, 
                sizeHitBox, 
                0,
                Vector2.left,
                0,
                playerLayer);
            return hitBox.collider != null;
        }
        
        private void OnDrawGizmos()
        {
            var bounds = enemy.GetColl2D.bounds;
            var originBox = bounds.center + enemy.transform.right * (hitBoxRangeX * enemy.transform.localScale.x * hitBoxDistance);
            var sizeHitBox = new Vector3(hitBoxRangeX * bounds.size.x, hitBoxRangeY * bounds.size.y, bounds.size.z);
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(originBox,sizeHitBox);
        }
    }
}