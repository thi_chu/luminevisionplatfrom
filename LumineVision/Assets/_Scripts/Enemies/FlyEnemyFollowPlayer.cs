using System.Collections.Generic;
using UnityEngine;

namespace _Scripts.Enemies
{
    public class FlyEnemyFollowPlayer : MonoBehaviour
    {
        [SerializeField] private List<EnemyFlyMovement> listEnemy;

        private void OnTriggerEnter2D(Collider2D coll)
        {
            if (coll.CompareTag("Player"))
            {
                foreach (var enemy in listEnemy)
                {
                    enemy.isChaseToPlayer = true;
                }
            }
        }

        private void OnTriggerExit2D(Collider2D coll)
        {
            if (coll.CompareTag("Player"))
            {
                foreach (var enemy in listEnemy)
                {
                    enemy.isChaseToPlayer = false;
                }
            }
        }
    }
}
