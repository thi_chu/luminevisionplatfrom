using System.Collections.Generic;
using UnityEngine;

namespace _Scripts.Enemies
{
    public class EnemyFlyMovement : MonoBehaviour
    {
        [SerializeField] private GameObject player;
        [SerializeField] private EnemyStatus controller;
        [SerializeField] private List<GameObject> movePoints;
        [SerializeField] private float speed;
        [SerializeField] private float idleDuration;
        [SerializeField] private float stoppingDistance;

        [HideInInspector] public bool isChaseToPlayer;

        private bool _isWaiting;
        private float _idleTimer;
        private int _currentPointIndex;
        private Quaternion _initRotation;


        // Start is called before the first frame update
        void Start()
        {
            player = GameObject.FindGameObjectWithTag("Player");
            isChaseToPlayer = false;
            _initRotation = transform.rotation;
        }

        // Update is called once per frame
        void Update()
        {
            if (player == null)
                return;
            
            if (controller.IsDead)
                return;

            if (isChaseToPlayer)
            {
                MoveToPlayer();
            }
            else
            {
                transform.rotation = _initRotation;
                if (_isWaiting)
                {
                    NextPoint();
                }
                else
                {
                    MoveDefault();
                }
            }
        }

        /// <summary>
        /// di chuyển theo palyer
        /// </summary>
        private void MoveToPlayer()
        {
            var direction = player.transform.position - transform.position;
            ;
            float rotation = Mathf.Atan2(-direction.y, -direction.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0, 0, rotation - 90);
            if (Vector2.Distance(controller.GetEnemy.position, player.transform.position) >= stoppingDistance)
            {
                controller.GetEnemy.position = Vector2.MoveTowards(
                    controller.GetEnemy.position,
                    player.transform.position,
                    speed * Time.deltaTime);
            }
        }

        /// <summary>
        /// di chuyển không player
        /// </summary>
        private void MoveDefault()
        {
            controller.GetEnemy.position = Vector2.MoveTowards(
                controller.GetEnemy.position,
                movePoints[_currentPointIndex].transform.position,
                speed * Time.deltaTime);
            if (Vector2.Distance(movePoints[_currentPointIndex].transform.position, controller.GetEnemy.position) < .1f)
            {
                _idleTimer = 0;
                _isWaiting = true;
            }
        }

        /// <summary>
        /// Di chuyển tới vị trí tiếp theo.
        /// Sử dụng khi di chuyển khong theo player
        /// </summary>
        private void NextPoint()
        {
            _idleTimer += Time.deltaTime;
            if (_idleTimer >= idleDuration)
            {
                _isWaiting = !_isWaiting;
                if (Vector2.Distance(movePoints[_currentPointIndex].transform.position, controller.GetEnemy.position) <
                    .1f)
                {
                    _currentPointIndex++;
                    if (_currentPointIndex >= movePoints.Count)
                    {
                        _currentPointIndex = 0;
                    }
                }
            }
        }
    }
}