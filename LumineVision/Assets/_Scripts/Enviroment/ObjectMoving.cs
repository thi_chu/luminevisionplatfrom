using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Scripts.Enviroment
{
    public class ObjectMoving : MonoBehaviour
    {
        [SerializeField] private List<GameObject> movePoints;
        [SerializeField] private float speed;
        [SerializeField] private bool isPlatfrom;

       

        private int _targetPoint;

        // Update is called once per frame
        void Update()
        {
            if (Vector2.Distance(movePoints[_targetPoint].transform.position, transform.position) < .1f)
            {
                _targetPoint++;
                if (_targetPoint >= movePoints.Count)
                {
                    _targetPoint = 0;
                }
            }
            transform.position = Vector2.MoveTowards(
                transform.position,
                movePoints[_targetPoint].transform.position,
                speed * Time.deltaTime);
        }

        private void OnCollisionEnter2D(Collision2D coll)
        {
            if (isPlatfrom)
            {
                if (coll.gameObject.CompareTag("Player"))
                {
                    coll.gameObject.transform.SetParent(transform);
                }
            }
        }

        private void OnCollisionExit2D(Collision2D coll)
        {
            if (isPlatfrom)
            {
                if (coll.gameObject.CompareTag("Player"))
                {
                    coll.gameObject.transform.SetParent(null);
                }
            }
        }
    }
}