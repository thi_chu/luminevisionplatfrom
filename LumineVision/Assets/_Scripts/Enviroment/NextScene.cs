using System;
using _Scripts.Manager;
using UnityEngine;

namespace _Scripts.Enviroment
{
    public class NextScene : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D coll)
        {
            if (coll.CompareTag("Player"))
            {
                GameManager.Instance.NextLevel2();
            }
        }
    }
}