using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace _Scripts.Manager
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;

        public int CurrentHp { get; set; }

        public int SumCoin { get; set; }

        // Start is called before the first frame update
        void Start()
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        public void StartGame()
        {
            CurrentHp =0;
            SumCoin = 0;
            SceneManager.LoadScene("Scenes/Level1");
        }

        public void NextLevel2()
        {
            SceneManager.LoadScene("Scenes/Level2");
        }

        public void MenuGame()
        {
            SceneManager.LoadScene("Scenes/MainMenu");
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}