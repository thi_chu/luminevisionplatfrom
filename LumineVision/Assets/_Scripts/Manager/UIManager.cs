using System;
using _Scripts.Player;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Manager
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager Instance;
        [Header("Audio")] 
        [SerializeField] private Slider musicSlide;
        [SerializeField] private Slider sfxSlider;

        [Header("Item")] 
        [SerializeField] private Text itemCount;
        [SerializeField] private Text sumCount;
        
        [Header("Panel")]
        [SerializeField] private GameObject gameOverPanel;

        private int _currentItem;


        // Start is called before the first frame update
        void Start()
        {
            Instance = this;
        }

        private void Update()
        {
            if (itemCount!= null)
            {
                itemCount.text = "x" + _currentItem;
                GameManager.Instance.SumCoin = _currentItem;
            }

            if (sumCount != null)
            {
                sumCount.text = "x" + GameManager.Instance.SumCoin;
            }
        }

        private void LateUpdate()
        {
            if (PlayerStatus.Instance.ChechDead())
            {
                GameOverPanel();
            }
            
        }

        public void UpdateItem()
        {
            _currentItem++;
        }

        public void OpenUI(GameObject ui)
        {
            ui.SetActive(true);
        }

        public void ColseUI(GameObject ui)
        {
            ui.SetActive(false);
        }

        public void GameOverPanel()
        {
            OpenUI(gameOverPanel);
        }

        //----------Audio------------//
        public void MusicVolume()
        {
            AudioManager.Instance.MusicVolume(musicSlide.value);
        }

        public void SFXVolume()
        {
            AudioManager.Instance.SFXVolume(sfxSlider.value);
        }
        
    }
}
