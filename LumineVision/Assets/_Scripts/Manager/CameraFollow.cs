using System;
using UnityEngine;

namespace _Scripts.Manager
{
    public class CameraFollow : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private Vector2 minBounds; 
        [SerializeField] private Vector2 maxBounds; 
        
        private Vector3 offset = new Vector3(0, 0, -12f);
        private Vector3 velocity = Vector3.zero;
        private float smoothTime = 0.25f;
        
        private void LateUpdate()
        {

            if (target!= null)
            {
                Vector3 targetPos = target.position + offset;
                targetPos.x = Mathf.Clamp(target.position.x,minBounds.x,maxBounds.x);
                targetPos.y = Mathf.Clamp(target.position.y,minBounds.y,maxBounds.y);
                transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref velocity, smoothTime);
            }
        }
    }
}
