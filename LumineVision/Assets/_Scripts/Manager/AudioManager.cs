using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _Scripts.Manager
{
    public class AudioManager : MonoBehaviour
    {
        public static AudioManager Instance;
        [SerializeField] private List<SoundModel> musicSounds, sfxSounds;
        [SerializeField] private AudioSource musicSource, sfxSource;

        public AudioSource GetmusicSource => musicSource;
        public AudioSource GetSFXSource => sfxSource;

        private void Start()
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            PlayMusic("Theme");
        }

        public void PlayMusic(string name)
        {
            SoundModel sound = musicSounds.Find(model => model.name == name);

            if (sound == null)
            {
                Debug.LogWarning("Sound Not Found!");
            }
            else
            {
                musicSource.clip = sound.clip;
                musicSource.Play();
            }
        }

        public void PlaySfx(string name)
        {
            SoundModel sound = sfxSounds.Find(model => model.name == name);

            if (sound == null)
            {
                Debug.LogWarning("Sound Not Found!");
            }
            else
            {
                sfxSource.PlayOneShot(sound.clip);
            }
        }

        public void MusicVolume(float volume)
        {
            musicSource.volume = volume;
        }

        public void SFXVolume(float volume)
        {
            sfxSource.volume = volume;
        }
    }

    [System.Serializable]
    public class SoundModel
    {
        public string name;
        public AudioClip clip;
    }
}