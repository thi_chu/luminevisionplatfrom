using System.Collections;
using _Scripts.Manager;
using _Scripts.UI;
using UnityEngine;

namespace _Scripts.Player
{
    public class PlayerStatus : MonoBehaviour
    {
        public static PlayerStatus Instance;

        [SerializeField] private GameObject player;

        [Header("Player Components")] [SerializeField]
        private HeathBar heathBar;

        [SerializeField] private Animator animator;
        [SerializeField] private Rigidbody2D rgb2D;
        [SerializeField] private Collider2D coll2D;

        [Header("Optinal")] [SerializeField] private int maxHp;
        [SerializeField] private float knockBackForce;
        [SerializeField] private float knockBackForceUp;
        [SerializeField] private float knockBackDuration = 0.2f;

        private int _currentHp;
        private bool _isKnockBack;
        private bool _isDead;
        private float _knocklbackTimer;

        public GameObject GetPlayer => player;
        public Animator GetAnimator => animator;
        public Rigidbody2D GetRgb2D => rgb2D;
        public Collider2D GetColl2D => coll2D;

        public bool IsMove { get; set; }
        

        // Start is called before the first frame update
        void Start()
        {
            Instance = this;
            _isKnockBack = false;
            IsMove = true;
            _isDead = false;
            _knocklbackTimer = knockBackDuration;
            _currentHp = GameManager.Instance.CurrentHp <= 0 ? maxHp : GameManager.Instance.CurrentHp;

            heathBar.UpdateHp(_currentHp, maxHp);
        }

        private void Update()
        {
            if (_isDead)
            {
                return;
            }

            if (_isKnockBack)
            {
                _knocklbackTimer -= Time.deltaTime;
                if (_knocklbackTimer <= 0)
                {
                    _isKnockBack = false;
                    _knocklbackTimer = knockBackDuration;
                    rgb2D.velocity = Vector2.zero;
                }
            }
        }

        private void OnTriggerEnter2D(Collider2D coll)
        {
            if (coll.CompareTag("Enemy") || coll.CompareTag("Trap"))
            {
                Vector2 knockbackDir = (transform.position - coll.transform.position).normalized;
                _isKnockBack = true;
                rgb2D.AddForce(
                    new Vector2(knockbackDir.x * knockBackForce, knockBackForceUp),
                    ForceMode2D.Impulse);
                TakeDamage(10);
            }

            if (coll.CompareTag("HeathItem"))
            {
                if (_currentHp < maxHp)
                {
                    _currentHp += 10;
                    Debug.LogWarning("hp: " + _currentHp);
                    heathBar.UpdateHp(_currentHp, maxHp);
                    GameManager.Instance.CurrentHp = _currentHp;
                }

                AudioManager.Instance.PlaySfx("HeathItem");
                Destroy(coll.gameObject);
            }

            if (coll.CompareTag("VisionItem"))
            {
                AudioManager.Instance.PlaySfx("Item");
                Destroy(coll.gameObject);
                UIManager.Instance.UpdateItem();
            }
        }

        public void TakeDamage(int dmg)
        {
            if (_isDead)
            {
                return;
            }

            _currentHp -= dmg;
            heathBar.UpdateHp(_currentHp, maxHp);
            StartCoroutine(Hurt());
            if (ChechDead())
            {
                animator.SetTrigger("die");
                Die();
                return;
            }
            animator.SetTrigger("hurt");
        }

        private void Die()
        {
            if (!_isDead)
            {
                _isDead = true;
                rgb2D.constraints = RigidbodyConstraints2D.FreezeAll;
                coll2D.enabled = false;
                AudioManager.Instance.GetmusicSource.Stop();
                AudioManager.Instance.PlaySfx("GameOver");
            }
        }

        public bool ChechDead()
        {
            return _currentHp <= 0;
        }
        
        public void CanMove()
        {
            IsMove = true;
        }

        private IEnumerator Hurt()
        {
            IsMove = false;
            yield return new WaitForSeconds(_knocklbackTimer);
            IsMove = true;

        }
    }
}