using System.Collections;
using _Scripts.Manager;
using UnityEngine;

namespace _Scripts.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private PlayerStatus status;
        [Header("Optional")] 
        [SerializeField] private float speed = 7f;
        [SerializeField] private float jumpForce = 10f;
        [SerializeField] private LayerMask groudLayer;
        [SerializeField] private TrailRenderer trailRend;

        private float _movePos;
        private bool _isDoubleJump;
        private static readonly int MoveState = Animator.StringToHash("moveState");

        //Dash skill
        private bool _canDash = true;
        private bool _isDashing;
        private float _dashPower = 20f;
        private float _dashingTime = 0.2f;
        private float _dashingCooldown = 1f;

        private enum MovementSate
        {
            Idle,
            Running,
            Jumping,
            Falling,
            Dashing,
        }

        private MovementSate _sate;

        // Update is called once per frame
        private void Update()
        {
            if (status.ChechDead()) return;
            if (_isDashing) return;
            if (!status.IsMove) return;

            status.GetRgb2D.velocity = new Vector2(_movePos, status.GetRgb2D.velocity.y);

            Movement();

            MovingAnimationUpdate();
        }

        private void MovingAnimationUpdate()
        {
            if (CheckGround())
            {
                if (status.GetRgb2D.velocity.x != 0)
                {
                    _sate = MovementSate.Running;
                }
                else
                {
                    _sate = MovementSate.Idle;
                }
            }

            if (status.GetRgb2D.velocity.y > .1f)
            {
                _sate = MovementSate.Jumping;
            }
            else if (status.GetRgb2D.velocity.y < -.1f)
            {
                _sate = MovementSate.Falling;
            }

            status.GetAnimator.SetInteger(MoveState, (int) _sate);
        }

        private void Movement()
        {
            var scale = transform.localScale;
            if (_movePos != 0)
            {
                if (_movePos < 0)
                {
                    transform.localScale =
                        new Vector3(-1 * Mathf.Abs(scale.x), scale.y, scale.z);
                }
                else
                {
                    transform.localScale =
                        new Vector3(1 * Mathf.Abs(scale.x), scale.y, scale.z);
                }
            }
        }

        public void MoveLeft()
        {
            _movePos = -speed;
        }

        public void MoveRight()
        {
            _movePos = speed;
        }

        public void NoMove()
        {
            _movePos = 0;
        }

        public void DashSkill()
        {
            if (_canDash)
            {
                _sate = MovementSate.Dashing;
                status.GetAnimator.SetInteger(MoveState, (int) _sate);
                StartCoroutine(Dash());
            }
        }

        public void Jump()
        {
            if (CheckGround())
            {
                status.GetRgb2D.velocity = new Vector2(status.GetRgb2D.velocity.x, jumpForce);
                _isDoubleJump = false;
                AudioManager.Instance.PlaySfx("Jump");
            }
            else if (_isDoubleJump == false)
            {
                status.GetRgb2D.velocity = new Vector2(status.GetRgb2D.velocity.x, jumpForce * 0.57f);
                _isDoubleJump = true;
            }
        }

        private bool CheckGround()
        {
            var bound = status.GetColl2D.bounds;
            var groundHitbox = Physics2D.BoxCast(
                bound.center,
                bound.size,
                0,
                Vector2.down,
                0.1f,
                groudLayer);
            return groundHitbox;
        }

        private IEnumerator Dash()
        {
            _canDash = false;
            _isDashing = true;
            // Lưu giá trị trọng lực ban đầu và tắt nó trong quá trình dash
            float originalGravity = status.GetRgb2D.gravityScale;
            status.GetRgb2D.gravityScale = 0f;
            // Đặt vận tốc để dash
            status.GetRgb2D.velocity = new Vector2(transform.localScale.x * _dashPower, 0f);
            trailRend.emitting = true;
            yield return new WaitForSeconds(_dashingTime);
            // Reset lại trail và trọng lực
            trailRend.emitting = false;
            status.GetRgb2D.gravityScale = originalGravity;
            _isDashing = false;
            // Chờ đợi thời gian cooldown trước khi có thể dash tiếp
            yield return new WaitForSeconds(_dashingCooldown);
            _canDash = true;
        }
    }
}