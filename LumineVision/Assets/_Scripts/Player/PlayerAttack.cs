using System;
using _Scripts.Enemies;
using _Scripts.Manager;
using UnityEngine;

namespace _Scripts.Player
{
    public class PlayerAttack : MonoBehaviour
    {
        [SerializeField] private Transform attackPoint;
        [SerializeField] private float attackPointRadius;
        [SerializeField] private LayerMask enemyLayer;

        private float _atkCooldown;
        private float _cooldownTime = 0.75f;

        // Start is called before the first frame update
        void Start()
        {
            _atkCooldown = _cooldownTime;
        }

        // Update is called once per frame
        void Update()
        {
            _atkCooldown -= Time.deltaTime;
        }

        public void Attack()
        {

            if (_atkCooldown <= 0)
            {
                if (PlayerStatus.Instance.ChechDead()) return;
            
                PlayerStatus.Instance.GetAnimator.SetTrigger("attack");
                AudioManager.Instance.PlaySfx("Attack");
                var enemy = Physics2D.OverlapCircle(attackPoint.position, attackPointRadius, enemyLayer);
                if (enemy)
                {
                    // takeDamage enemy
                    enemy.GetComponent<EnemyStatus>().TakeDamage(10);
                }

                _atkCooldown = _cooldownTime;
            }
        }


        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(attackPoint.position, attackPointRadius);
        }
    }
}